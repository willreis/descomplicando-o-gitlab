# Descomplicando o GitLab 

Treinamento Descomplicando o Gitlab criado ao vivo


### Day-1

```bash
- Entendemos o que é o Git
- Entendemos o que é o Gitlab
- Como criar um Grupo on Gitlab 
- Como criar um repositŕio Git
- Comando básicos para manipulação de arquivos e diretórios no Git
- Como criar um branch
- Como criar um Merge Request 
- Como adicionar um Membro no projeto
- Como fazer o merge no Master/Main
```

